/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jayvy
 */
public class Subtract extends Calculator {
    
       public Subtract ( double operand1, double operand2){
        
    super(operand1, operand2);
        
    }
    
    public double calculate(){
     return operand1 - operand2;   
    }
}
